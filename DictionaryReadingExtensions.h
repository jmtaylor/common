//
//  DictionaryReadingExtensions.h
//
//	Copyright 2014-2015 Jonathan Taylor. All rights reserved.
//
//	Extensions to NSDictionary for easy and typesafe access to key values
//

#ifndef Spim_Interface_DictionaryReadingExtensions_h
#define Spim_Interface_DictionaryReadingExtensions_h

@interface NSDictionary (KeyReading)

#include "DictionaryReadingExtensionsImplementation.h"

@end

#endif
