//
//  DictionaryReadingExtensions.mm
//
//	Copyright 2014-2015 Jonathan Taylor. All rights reserved.
//
//	Extensions to NSDictionary for easy and typesafe access to key values
//

#import "DictionaryReadingExtensions.h"

@implementation NSDictionary (KeyReading)

#define DICTIONARY_CLASS NSDictionary
#include "DictionaryReadingExtensionsImplementation.mm"

@end
